## Quotes-manager API
## Installation

- Clone this repo to directory on your server
- Run "composer install" and install all needed dependencies
- Rename .env.example file to .env and specify your database connection details
- Run migrations by "php artisan migrate"
- Run seeder by "php artisan db:seed"
- Generate app key by "php artisan key:generate"

## Postman Collection
https://documenter.getpostman.com/view/1094881/quotes-manager-api/RWEmHw11#intro
##PHP

1. Написать функцию нахождения максимального числа в массиве любой вложенности.
Не изменять входных аргументов функции.

```
<?php
 
function myMax($xs) {
    $max = 0;
    foreach ($xs as $item) {
        if (is_array($item)) {
            $value = myMax($item);
        } else {
            $value = $item;
        }
        $max = $value > $max ? $value : $item;
    }
    return $max;
}
echo myMax([1, 2, [3, 4, [7, 8, [9, 10]]]]); // 10

```
2. Написать функцию для сортировки массива.
```
<?php
    $list = [20, 10, -5, 0, 5, -45, 10, 555];
    function mySort($list) {
        $result = [];
        $arrayCount = count($list);
        for ($i = 0; $i < $arrayCount; $i++) {
            for ($j = $i + 1; $j < $arrayCount; $j++) {
                $temp = 0;
                if ($list[$i] > $list[$j]) {
                    $temp = $list[$i];
                    $list[$i] = $list[$j];
                    $list[$j] = $temp;
                }
            }
        }

        return $list;
    }
    print_r(mySort($list));

```

##JS
1.Есть массив [10, 20, 30].Поменяйте местами 0 и 1 элементы, чтобы получилось [20, 10, 30].
```
function changeElements(list) {
    [list[0], list[list.length - 1]] = [list[list.length - 1], list[0]];
    return list;
}
const list = [10, 20, 30, 1];
changeElements(list);
console.log(list);

```
2.Есть массив [30, -5, 0, 10, 5].
Напишите функцию сортировки от наименьшего к наибольшему, результат [-5, 0, 5, 10, 30] . Не используйте стандартную функцию sort.
```
const list = [30, -5, 0, 10, 5, 45, -45];

function mySort(list) {
  var length = list.length;
  for (var i = 0; i < length; i++) {
    for (var j = 0; j < (length - i - 1); j++) { 
      if(list[j] > list[j+1]) {
        var tmp = list[j];  
        list[j] = list[j+1]; 
        list[j+1] = tmp; 
      }
    }        
  }
  return list;
}
mySort(list);
console.log(list);
```

3.Напишите свою реализацию bind.
```
var func1 = function(message) {
  this(message);
}

var func2 = func1.bind(alert);
func2('Test'); // alert 'Test'

function myBind(func, context) {
  return function() {
    return func.apply(context, arguments);
  };
}

var func3 = myBind(func1, alert);
func3('Test'); // alert 'Test'
```