<?php

use Illuminate\Database\Seeder;

class QuotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->insert([
            'authors_id' => 1,
            'content' => str_random(50),
        ]);

        DB::table('quotes')->insert([
            'authors_id' => 1,
            'content' => str_random(50),
        ]);

        DB::table('quotes')->insert([
            'authors_id' => 2,
            'content' => str_random(50),
        ]);

        DB::table('quotes')->insert([
            'authors_id' => 3,
            'content' => str_random(50),
        ]);

        DB::table('quotes')->insert([
            'authors_id' => 3,
            'content' => str_random(50),
        ]);

        DB::table('quotes')->insert([
            'authors_id' => 4,
            'content' => str_random(50),
        ]);

        DB::table('quotes')->insert([
            'authors_id' => 5,
            'content' => str_random(50),
        ]);
    }
}
