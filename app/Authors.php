<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'surname'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotes()
    {
        return $this->hasMany(Quotes::class);
    }
}
