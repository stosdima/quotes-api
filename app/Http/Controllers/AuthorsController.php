<?php

namespace App\Http\Controllers;

use App\Authors;
use App\Http\Resources\AuthorsResource;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if ($request->input('perPage')) {
            $response = AuthorsResource::collection(Authors::paginate($request->input('perPage')));
        } else {
            $response = AuthorsResource::collection(Authors::all());
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return AuthorsResource
     */
    public function store(Request $request)
    {
        $author = Authors::create([
            'name' => $request->name,
            'surname' => $request->surname,
        ]);

        return new AuthorsResource($author);
    }

    /**
     * @param Authors $authors
     * @return AuthorsResource
     */
    public function show($id)
    {
        $author = Authors::findOrFail($id);

        return new AuthorsResource($author);
    }

    /**
     * @param Request $request
     * @param Authors $authors
     * @return AuthorsResource
     */
    public function update(Request $request, Authors $authors, $id)
    {
        $author = $authors->findOrFail($id);
        $author->update([
            'name' => $request->name,
            'surname' => $request->surname,
        ]);

        return new AuthorsResource($author);
    }

    /**
     * @param Authors $authors
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Authors $authors, $id)
    {
        $author = $authors->findOrFail($id);
        if (count($author->quotes)) {
            $author->quotes()->delete();
        }
        $author->delete();

        return response()->json(null, 204);
    }
}
