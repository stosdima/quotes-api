<?php

namespace App\Http\Controllers;

use App\Authors;
use App\Quotes;
use App\Http\Resources\QuotesResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class QuotesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        if ($request->input('perPage')) {
            $response = QuotesResource::collection(Quotes::paginate($request->input('perPage')));
        } else {
            $response = QuotesResource::collection(Quotes::all());
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return QuotesResource
     */
    public function store(Request $request): QuotesResource
    {
        if (Authors::findOrFail($request->authors_id)) {
            $quote = Quotes::create([
                'content' => $request->content,
                'authors_id' => $request->authors_id,
            ]);

            return new QuotesResource($quote);
        } else {
            return response()->json(['error' => 'Bad request', 'status' => 400], 400);
        }

    }

    /**
     * @param $id
     * @return QuotesResource
     */
    public function show($id): QuotesResource
    {
        $quote = Quotes::findOrFail($id);

        return new QuotesResource($quote);
    }

    /**
     * @param Request $request
     * @param Quotes $quotes
     * @param $id
     * @return QuotesResource
     */
    public function update(Request $request, Quotes $quotes, int $id): QuotesResource
    {
        if (Authors::findOrFail($request->authors_id)) {
            $quote = $quotes->findOrFail($id);
            $quote->update([
                'content' => $request->content,
                'authors_id' => $request->authors_id,
            ]);

            return new QuotesResource($quote);
        } else {
            return response()->json(['error' => 'Bad request', 'status' => 400], 400);
        }
    }

    /**
     * @param Quotes $quotes
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Quotes $quotes, int $id): JsonResponse
    {
        $quote = $quotes->findOrFail($id);
        if ($quote->delete()) {
            return response()->json(['success' => 'Quote was deleted'], 204);
        }
    }

    public function searchByAuthor(Quotes $quotes, Request $request)
    {
        if ($request->input('name')) {
            $name = $request->input('name');
            $quotes = $quotes->with('Authors')->whereHas('Authors', function ($q) use ($name) {
                $q->where('name', 'like', "%{$name}%");
            })->get();

            return response()->json(['data' => $quotes], 200);
        } else {
            return response()->json(['error' => 'Bad request', 'status' => 400], 400);
        }
    }
}
