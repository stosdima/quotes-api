<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['content', 'authors_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authors()
    {
        return $this->belongsTo(Authors::class);
    }
}
